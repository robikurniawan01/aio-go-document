import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListApisService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  searchEmployee(searchTerm: string, searchBy: string): Observable<any[]> {
    const body = { searchTerm, searchBy };
    return this.http.post<any[]>(`${this.apiUrl}/employee/searchEmployee`, body);
  }

  getAllRole(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/role/getAllRole`);
  }
  getAllDepartement(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/departement/getAllDepartement`);
  }
  getAllAuthorizationPaginate(page: number, itemsPerPage: number): Observable<any> {
    const body = { page, itemsPerPage };
    return this.http.post<any>(`${this.apiUrl}/auth/getAllAuthorizationJoinPaginate`, body);
  }
  getAllAuthorization(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/auth/getAllAuth`);
  }
  deleteAuthById(id: number): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/auth/deleteAuthById/${id}`);
  }
  createAuth(form: any): Observable<any> {
    const body = form;
    return this.http.post<any>(`${this.apiUrl}/auth/createAuth`, body);
  }
  updateAuth(id: number, form: any): Observable<any> {
    const body = form;
    return this.http.put<any>(`${this.apiUrl}/auth/updateAuth/${id}`, body);
  }
  getAllAttendanceMeeting(meetingIdNumber: number): Observable<any> {
    const meetingId = { meetingId: meetingIdNumber };
    return this.http.post<any>(`${this.apiUrl}/attendance/getAllAttendance`, meetingId);
  }
  updateInsertAttendance(meetingId: number, dataAttedance: any[]): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/attendance/upSetAttedance`, { meetingId, data: dataAttedance });
  }
}
