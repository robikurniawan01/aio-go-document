import { TestBed } from '@angular/core/testing';

import { ListApisService } from './list-apis.service';

describe('ListApisService', () => {
  let service: ListApisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListApisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
