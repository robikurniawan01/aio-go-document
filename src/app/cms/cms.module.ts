import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbDropdownModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { SimplebarAngularModule } from 'simplebar-angular';
import { LanguageService } from '../core/services/language.service';
import { TranslateModule } from '@ngx-translate/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { FlatpickrModule } from 'angularx-flatpickr';
import { LightboxModule } from 'ngx-lightbox';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

import { CmsRoutingModule } from './cms-routing.module';

import { LayoutComponent } from './layouts/layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

// Component pages
import { VerticalComponent } from './layouts/vertical/vertical.component';
import { TopbarComponent } from './layouts/topbar/topbar.component';
import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { RightsidebarComponent } from './layouts/rightsidebar/rightsidebar.component';
import { HorizontalComponent } from './layouts/horizontal/horizontal.component';
import { HorizontalTopbarComponent } from './layouts/horizontal-topbar/horizontal-topbar.component';
import { TwoColumnComponent } from './layouts/two-column/two-column.component';
import { TwoColumnSidebarComponent } from './layouts/two-column-sidebar/two-column-sidebar.component';
import { ListPermissionComponent } from './pages/master-permission/list-permission/list-permission.component';
import { ListRoleComponent } from './pages/master-role/list-role/list-role.component';
import { AddRoleComponent } from './pages/master-role/add-role/add-role.component';
import { UpdateRoleComponent } from './pages/master-role/update-role/update-role.component';
import { ListUserComponent } from './pages/master-user/list-user/list-user.component';
import { AddUserComponent } from './pages/master-user/add-user/add-user.component';
import { UpdateUserComponent } from './pages/master-user/update-user/update-user.component';
import { TherapeuticalComponent } from './pages/master/therapeutical/therapeutical.component';
import { SharedModule } from './shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { ListArticleComponent } from './pages/article/list-article/list-article.component';
import { AddArticleComponent } from './pages/article/add-article/add-article.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';



import { CategoryComponent } from './pages/master/category/category.component';
import { TagComponent } from './pages/master/tag/tag.component';
import { TeamComponent } from './pages/master/team/team.component';
import { CommunityComponent } from './pages/master/community/community.component';
import { PermissionComponent } from './pages/master/permission/permission.component';
import { AuthorizationComponent } from './pages/master/authorization/authorization.component';
import { DoctorComponent } from './pages/master/doctor/doctor.component';
import { TaskListComponent } from './pages/task-list/task-list.component';
import { StatusArticlePipe } from '../core/services/custom.pipe';
import { DepartmentComponent } from './pages/master/department/department.component';
import { MeetingManagementComponent } from './pages/master/meeting-management/meeting-management';
import { ConfirmAttendanceComponent } from './pages/master/confirm-attendance/confirm-attendance';


//Pipe

// import { StatusArticlePipe } from './../core/services/custom.pipe'



@NgModule({
  declarations: [
    //Custom Pipe,
    StatusArticlePipe,
    //layout
    LayoutComponent,
    VerticalComponent,
    TopbarComponent,
    SidebarComponent,
    FooterComponent,
    RightsidebarComponent,
    HorizontalComponent,
    HorizontalTopbarComponent,
    TwoColumnComponent,
    TwoColumnSidebarComponent,
    DashboardComponent,
    //master
    ListPermissionComponent,
    ListRoleComponent,
    AddRoleComponent,
    AuthorizationComponent,
    UpdateRoleComponent,
    ListUserComponent,
    AddUserComponent,
    UpdateUserComponent,
    TherapeuticalComponent,
    CategoryComponent,
    TagComponent,
    TeamComponent,
    CommunityComponent,
    PermissionComponent,
    DoctorComponent,
    ListArticleComponent,
    AddArticleComponent,
    TaskListComponent,
    DepartmentComponent,
    MeetingManagementComponent,
    ConfirmAttendanceComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CmsRoutingModule,
    NgbDropdownModule,
    NgbNavModule,
    DropzoneModule,
    SimplebarAngularModule,
    CKEditorModule,
    TranslateModule,
    NgSelectModule,
    FlatpickrModule,
    LightboxModule,
    MatTableModule,
    MatButtonModule,
    MatSortModule,
    MatPaginatorModule,
    SharedModule
  ],
  providers: [LanguageService]
})
export class CmsModule { }
