import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ListPermissionComponent } from './pages/master-permission/list-permission/list-permission.component';
import { ListRoleComponent } from './pages/master-role/list-role/list-role.component';
import { AddRoleComponent } from './pages/master-role/add-role/add-role.component';
import { UpdateRoleComponent } from './pages/master-role/update-role/update-role.component';
import { ListUserComponent } from './pages/master-user/list-user/list-user.component';
import { AddUserComponent } from './pages/master-user/add-user/add-user.component';
import { UpdateUserComponent } from './pages/master-user/update-user/update-user.component';
import { TherapeuticalComponent } from './pages/master/therapeutical/therapeutical.component';
import { CategoryComponent } from './pages/master/category/category.component';
import { TagComponent } from './pages/master/tag/tag.component';
import { TeamComponent } from './pages/master/team/team.component';
import { CommunityComponent } from './pages/master/community/community.component';
import { PermissionComponent } from './pages/master/permission/permission.component';
import { AuthorizationComponent } from './pages/master/authorization/authorization.component';
import { DoctorComponent } from './pages/master/doctor/doctor.component';
import { ListArticleComponent } from './pages/article/list-article/list-article.component';
import { AddArticleComponent } from './pages/article/add-article/add-article.component';
import { TaskListComponent } from './pages/task-list/task-list.component';
import { DepartmentComponent } from './pages/master/department/department.component';
import { MeetingManagementComponent } from './pages/master/meeting-management/meeting-management';
import { ConfirmAttendanceComponent } from './pages/master/confirm-attendance/confirm-attendance';

const routes: Routes = [
  {
    path: "",
    component: DashboardComponent
  },
  {
    path: "post/list",
    component: ListArticleComponent
  },
  {
    path: "post/add",
    component: AddArticleComponent
  },
  {
    path: "therapeutical",
    component: TherapeuticalComponent
  },
  {
    path: "permission",
    component: PermissionComponent
  },
  {
    path: "category",
    component: CategoryComponent
  },
  {
    path: "tag",
    component: TagComponent
  },
  {
    path: "doctor",
    component: DoctorComponent
  },
  {
    path: "team",
    component: TeamComponent
  },
  {
    path: "community",
    component: CommunityComponent
  },
  {
    path: "authorization",
    component: AuthorizationComponent
  },
  {
    path: "add-role",
    component: AddRoleComponent
  },
  {
    path: "update-role",
    component: UpdateRoleComponent
  },
  {
    path: "user",
    component: ListUserComponent
  },
  {
    path: "add-user",
    component: AddUserComponent
  },
  {
    path: "update-user",
    component: UpdateUserComponent
  },
  {
    path: "task-list",
    component: TaskListComponent
  },
  {
    path: "department",
    component: DepartmentComponent
  },
  {
    path: "meeting-management",
    component: MeetingManagementComponent
  },
  {
    path: "confirm-attendance/:id",
    component: ConfirmAttendanceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule { }
