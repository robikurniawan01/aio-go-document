import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [

  // {
  //   id: 1,
  //   label: 'MENUITEMS.MENU.TEXT',
  //   isTitle: true
  // },
  // {
  //   id: 100,
  //   label: 'Task List',
  //   link: '/task-list',
  //   icon: 'ri-todo-line',
  // },
  // {
  //   id: 2,
  //   label: 'Dashboard',
  //   icon: 'ri-dashboard-fill',

  // },
  // {
  //   id: 3,
  //   label: 'Post',
  //   icon: 'ri-send-plane-fill',
  //   subItems: [
  //     {
  //       id: 9,
  //       label: 'List Post',
  //       link: '/post/list',
  //       parentId: 3
  //     },
  //     {
  //       id: 10,
  //       label: 'MENUITEMS.APPS.LIST.CHAT',
  //       link: '/chat',
  //       parentId: 3
  //     }
  //   ]
  // },
  // {
  //   id: 4,
  //   label: 'Media',
  //   icon: 'ri-gallery-line',
  //   subItems: [
  //     {
  //       id: 9,
  //       label: 'MENUITEMS.APPS.LIST.CALENDAR',
  //       link: '/calendar',
  //       parentId: 3
  //     },
  //     {
  //       id: 10,
  //       label: 'MENUITEMS.APPS.LIST.CHAT',
  //       link: '/chat',
  //       parentId: 3
  //     }
  //   ]
  // },
  // {
  //   id: 5,
  //   label: 'Comment',
  //   icon: ' ri-message-2-line',
  //   link: '/comment',
  // },
  // {
  //   id: 5,
  //   label: 'Approved',
  //   icon: 'ri-checkbox-circle-line',
  //   subItems: [
  //     {
  //       id: 9,
  //       label: 'MENUITEMS.APPS.LIST.CALENDAR',
  //       link: '/calendar',
  //       parentId: 3
  //     },
  //     {
  //       id: 10,
  //       label: 'MENUITEMS.APPS.LIST.CHAT',
  //       link: '/chat',
  //       parentId: 3
  //     }
  //   ]
  // },
  {
    id: 6,
    label: 'Master Data',
    isTitle: true,
  },
  // {
  //   id: 101,
  //   label: 'Master Therapeutical',
  //   icon: 'ri-list-settings-fill',
  //   link: '/therapeutical'
  // },
  {
    id: 9,
    label: 'Master Authorization',
    icon: 'ri-group-line',
    link: '/authorization'
  },
  {
    id: 7,
    label: 'Master Department',
    icon: 'ri-stack-line',
    link: '/department'
  },
  // {
  //   id: 7,
  //   label: 'Master Permission',
  //   icon: 'ri-newspaper-line',
  //   link: '/permission'
  // },
  // {
  //   id: 8,
  //   label: 'Master Role',
  //   icon: 'ri-shield-user-line',
  //   link: '/role'
  // },

  // {
  //   id: 10,
  //   label: 'Master Category',
  //   icon: 'ri-stack-line',
  //   link: '/category'
  // },
  // {
  //   id: 11,
  //   label: 'Master Tag',
  //   icon: 'ri-price-tag-3-line',
  //   link: '/tag'
  // },
  // {
  //   id: 12,
  //   label: 'Master Doctor',
  //   icon: 'ri-shield-user-line',
  //   link: '/doctor'
  // },
  // {
  //   id: 13,
  //   label: 'Master Team',
  //   icon: 'ri-team-line',
  //   link: '/team'
  // },
  // {
  //   id: 14,
  //   label: 'Master Community',
  //   icon: 'ri-community-fill',
  //   link: '/community'
  // },
  {
    id: 6,
    label: 'Document',
    isTitle: true,
  },
  {
    id: 15,
    label: 'Generate Document Number',
    icon: 'ri-calendar-event-fill',
    subItems: [
      {
        id: 10,
        label: 'Reminder Daily update',
        link: '/chat',
        parentId: 15
      },
      {
        id: 9,
        label: 'Daily Update',
        link: '/meeting-management',
        parentId: 15
      },
      {
        id: 9,
        label: 'Daily Meeting MoM',
        link: '/meeting-management',
        parentId: 15
      }

    ]
  },
  // {
  //   id: 15,
  //   label: 'Weekly',
  //   icon: 'ri-calendar-todo-line',
  //   subItems: [
  //     {
  //       id: 10,
  //       label: 'Blast Invitation',
  //       link: '/chat',
  //       parentId: 15
  //     },
  //     {
  //       id: 9,
  //       label: 'Manage Weekly Meeting',
  //       link: '/meeting-management',
  //       parentId: 15
  //     }

  //   ]
  // },
  // {
  //   id: 15,
  //   label: 'Monthly',
  //   icon: 'ri-calendar-todo-fill',
  //   subItems: [
  //     {
  //       id: 10,
  //       label: 'Blast Invitation',
  //       link: '/chat',
  //       parentId: 15
  //     },
  //     {
  //       id: 9,
  //       label: 'Meeting Management',
  //       link: '/meeting-management',
  //       parentId: 15
  //     }

  //   ]
  // },

  // {
  //   id: 6,
  //   label: 'My Task',
  //   isTitle: true,
  // },
  // {
  //   id: 7,
  //   label: 'Daily Update',
  //   icon: 'ri-todo-line',
  //   link: '/department'
  // },


  // {
  //   id: 6,
  //   label: 'My Agenda',
  //   isTitle: true,
  // },
  // {
  //   id: 7,
  //   label: 'Meeting',
  //   icon: 'ri-slideshow-line',
  //   link: '/department'
  // },
];
