import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';

import { NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastsContainer } from './toasts/toasts.component';



@NgModule({
  declarations: [
    BreadcrumbsComponent,
    ToastsContainer,
  ],
  imports: [
    CommonModule,
    NgbToastModule
  ],
  exports: [
    BreadcrumbsComponent,
    ToastsContainer,
    CommonModule,
    NgbToastModule
  ]
})
export class SharedModule { }
