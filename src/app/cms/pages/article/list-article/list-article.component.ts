import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/cms/shared/toasts/toasts.service';
import { ApiService } from 'src/app/core/services/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.scss']
})
export class ListArticleComponent {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  // bread crumb items
  breadCrumbItems!: Array<{}>;

  constructor(private apiService: ApiService, private modalService: NgbModal, public toastService: ToastService){

  }

  displayedColumns: string[] = ['no', 'title', 'post_status', 'created_at', 'action'];
  searchTerm:string = "";
  dataSource = new MatTableDataSource<any>([]);


  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Posts' },
      { label: 'List Posts', active: true }
    ];
  }
  
  ngAfterViewInit(){
     // Assign MatPaginator
    this.getData();
  }

  getData() {
    // this.apiService.get(`article-post?page=${this.paginator.pageIndex + 1}&size=${this.paginator.pageSize}`).subscribe((res:any) => {
    this.apiService.get(`article-post`).subscribe((res:any) => {
      this.dataSource = new MatTableDataSource<any>(res.data.posts);
      this.dataSource.sort = this.sort; // Assign MatSort
      this.dataSource.paginator = this.paginator;
      // this.paginator.length = res.data.totalPosts
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onDelete(item:any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to delete this item!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        const id = item.id;
        this.apiService.put(`master/therapeutical/${id}`, { form_data: {status:'deleted'} }).subscribe(
          (res) => {
            this.getData();
            Swal.fire('Deleted!', 'The item has been deleted.', 'success');
          },
          (err) => {
            Swal.fire('Error', 'An error occurred while deleting the item.', 'error');
          }
        );
      }
    });
  }
}
