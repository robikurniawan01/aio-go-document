import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/cms/shared/toasts/toasts.service';
import { ApiService } from 'src/app/core/services/api.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// import { SimpleUploadAdapter } from '@ckeditor/ckeditor5-upload';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.scss']
})
export class AddArticleComponent {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  showContent: boolean = false;
  public jurnalFile : any = [];
  public jurnalReferences : any = [];

  
  // bread crumb items
  breadCrumbItems!: Array<{}>;
  public Editor = ClassicEditor;
  public editorData = '';
  public config = {
    // Your CKEditor configuration options go here
    // plugins: [SimpleUploadAdapter],
    // toolbar: ['ckfinder', 'imageUpload', '|', 'undo', 'redo'],
    // simpleUpload: {
    //   // The URL that the images are uploaded to.
    //   uploadUrl: 'http://example.com',

    //   // Enable the XMLHttpRequest.withCredentials property.
    //   withCredentials: true,

    //   // Headers sent along with the XMLHttpRequest to the upload server.
    //   headers: {
    //       'X-CSRF-TOKEN': 'CSRF-Token',
    //       Authorization: 'Bearer <JSON Web Token>'
    //   }
    // }
  };
  public metadata = {
    therapeuticals: [],
    categories: [],
    tags: [],
  }

  public submit = false;
  public user:any = "";

  selectValue = ['Alaska', 'Hawaii', 'California', 'Nevada', 'Oregon', 'Washington', 'Arizona', 'Colorado', 'Idaho', 'Montana', 'Nebraska', 'New Mexico', 'North Dakota', 'Utah', 'Wyoming', 'Alabama', 'Arkansas', 'Illinois', 'Iowa'];


  constructor(private apiService: ApiService, private modalService: NgbModal, public toastService: ToastService, private router: Router){

  }

  dataForm = new FormGroup({
    id: new FormControl(null),
    writer: new FormControl(null, Validators.required),
    content: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required),
    tags: new FormControl(''),
    isJurnal:  new FormControl(false),
    post_status: new FormControl(null, Validators.required),
    comment_status: new FormControl(null, Validators.required),
    highlight: new FormControl('', Validators.required),
    thumbnail: new FormControl(''),
    references : new FormControl(null),
    is_private: new FormControl(null, Validators.required),
    publish_date: new FormControl(null),
    slug_url: new FormControl(''),
    file: new FormControl(null),
    status: new FormControl('active', Validators.required),
    //metadata
    selectedTherapeuticals: new FormControl('active', Validators.required),
    selectedCategories: new FormControl('active', Validators.required),
  });

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Posts' },
      { label: 'Add Post', active: true }
    ];
    let currentUser = JSON.parse(localStorage.getItem('currentUser')?? '{}');
    this.dataForm.controls.writer.setValue(currentUser.name?? "Admin");
  }

  ngAfterViewInit(){
   this.getData();
 }

  getData() {
    this.apiService.get('article-post/metadata').subscribe((res:any) => {
      this.metadata = res.data;
    });
  }

  onFormSubmit(event: Event) {
    event.preventDefault();
    this.submit = true
    this.dataForm.controls.file.setValue(this.jurnalFile);
    this.dataForm.controls.references.setValue(this.jurnalReferences)
    //validation
    if(!this.dataForm.valid){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please fill out all required fields!',
      });
      console.log(this.dataForm.errors)
      return;
    }

    //build data
    let title = this.dataForm.controls.title.value?? "";
    let slug = title.toLowerCase().replace(/[^\w\s-]/g, '').trim().replace(/\s+/g, '-');
    this.dataForm.controls.slug_url.setValue(slug);
    const form_data = this.dataForm.value;
    const id = this.dataForm.value.id;
    console.log(form_data)
    
    //Add Data
    if(!id){
      this.apiService.post('article-post/', { form_data }).subscribe((res:any)=> {
        if(res.status) {
          // this.getData();
          Swal.fire('Data Inserted!', 'The item has been inserted', 'success');
          // this.modalService.dismissAll();
          this.router.navigate(['post/list'])
        } else {
          Swal.fire('Error', 'An error occurred while inserting the item.', 'error');
          console.log('error', res)
        }
      })


    //Edit Data
    // } else {
    //   this.apiService.put(`master/therapeutical/${id}`, { form_data }).subscribe((res:any)=> {
    //     if(res.status) {
    //       // this.getData();
    //       Swal.fire('Data Updated!', 'The item has been updated', 'success');
    //       this.modalService.dismissAll()
    //     } else {
    //       Swal.fire('Error', 'An error occurred while updating the item.', 'error');
    //       console.log('error', res)
    //     }
    //   })
    }
    
  }

  // onEditorReady(editor: any): void {
  //   editor.plugins.get('FileRepository').createUploadAdapter = (loader:any) => {
  //     return new MyUploadAdapter(loader);
  //   };
  // }



  onFileSelected(event: any, file: string) {
    const formData = new FormData();
    formData.append('file', event.target.files[0]);

    this.apiService.postFile('/storage/upload', formData)
      .subscribe((res: any) => {
        let myObject = {
          filename: res.filename,
          id : 0
        };
        let uniqueId = this.jurnalFile.length + 1;
        myObject.id = uniqueId;
        this.jurnalFile.push(myObject);
        console.log(this.jurnalFile)

        
      });
  }


  // removeObjectFromArray(arr: any[], propertyName: string, propertyValue: any): any[] {
  //   return arr.filter(item => item[propertyName] !== propertyValue);
  // }

  deleteFile(data:any) {
    // this.removeObjectFromArray(this.jurnalFile, 'id', id );
    this.jurnalFile = this.jurnalFile.filter((item: { id: any; }) => item.id !== data);
  }

  deleteRef(data:any) {
    this.jurnalReferences = this.jurnalReferences.filter((item: { id: any; }) => item.id !== data);

  }


  addReference() {
    console.log(this.dataForm.value.references);

    let myObject = {
      reference: this.dataForm.value.references,
      id : 0
    };
    let uniqueId = this.jurnalReferences.length + 1;
    myObject.id = uniqueId;
    this.jurnalReferences.push(myObject);

    this.dataForm.get('references')?.reset();
    
  }

}
// class MyUploadAdapter {
//   constructor(private loader:any) {}

//   upload(): Promise<any> {
//     return new Promise((resolve, reject) => {
//       // Implement your image upload logic here
//       // You can use libraries like Axios or Angular's HttpClient to send the image to your server
//     });
//   }

//   abort(): void {
//     // Abort the upload if needed
//   }
// }

