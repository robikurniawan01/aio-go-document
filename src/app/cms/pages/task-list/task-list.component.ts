import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ApiService } from 'src/app/core/services/api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  // bread crumb items
  imagePath = (item: any) => {
    return environment.imagePath + item;
  }

  breadCrumbItems = [
    { label: 'Apps' },
    { label: 'Task List', active: true }
  ];

  constructor(private apiService: ApiService) {

  }

  displayedColumns: string[] = ['type', 'subject', 'publish', 'area', 'writer', 'created_at', 'status','action'];
  searchTerm: string = "";
  dataSource = new MatTableDataSource<any>([]);

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.apiService.post('task-list/get-where', {
      role_id: JSON.parse(localStorage.getItem('currentUser')?? '{}').role_id,
      user_id: JSON.parse(localStorage.getItem('currentUser')?? '{}').email
    }).subscribe((res: any) => {
      this.dataSource = new MatTableDataSource<any>(res.data);
      this.dataSource.sort = this.sort; 
      this.dataSource.paginator = this.paginator; 
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  viewElement(item: any) {
    
  }

 



}