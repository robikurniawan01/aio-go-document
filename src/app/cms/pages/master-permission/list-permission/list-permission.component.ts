import { Component, OnInit, ViewChild, ViewEncapsulation, TemplateRef } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { DatePipe } from '@angular/common'
import { HttpClient, HttpResponse } from '@angular/common/http';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from '@angular/router';


@Component({
  selector: 'app-list-permission',
  templateUrl: './list-permission.component.html',
  styleUrls: ['./list-permission.component.scss']
})
export class ListPermissionComponent implements OnInit {

  public data: any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "Name";
  public sortOrder: string = "asc";
  public loading = false;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.get('master/permission').subscribe((data: any) => {
      this.data = data;
      console.log('ini data', this.data)
    });
  }

  onSort() {
  }
}
