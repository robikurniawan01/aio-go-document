import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ApiService } from 'src/app/core/services/api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ListApisService } from 'src/app/list-apis.service';


import Swal from 'sweetalert2';


@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  nameControl = new FormControl();
  autocompleteResults: any = [];
  // bread crumb items
  breadCrumbItems!: Array<{}>;

  constructor(private apiService: ApiService, private modalService: NgbModal, private listApiService: ListApisService) {

  }

  displayedColumns: string[] = ['employee_code', 'employee_name', 'name_departement','code_departement', 'role', 'created_at','status' , 'actions'];
  arrayDataEmployees: any = [];
  arrayDataRole: any = [];
  arrayDataDepartement: any = [];
  searchTerm: string = "";
  dataSource = new MatTableDataSource<any>([]);
  dataPermission = []
  page = 1; // Halaman default
  itemsPerPage = 10; // Jumlah item per halaman default
  searchBy: string = "employee_name";
  dataForm = new FormGroup({
    id: new FormControl(null),
    employee_code: new FormControl('', Validators.required),
    employee_name: new FormControl('', Validators.required),
    department_id: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    status: new FormControl('active', Validators.required),
  });

  ngOnInit() {
    this.page = 1; // Inisialisasi halaman pertama
    this.itemsPerPage = 10; // Inisialisasi jumlah item per halaman
    this.getData(this.page, this.itemsPerPage);
    this.breadCrumbItems = [
      { label: 'Apps' },
      { label: 'Master Authorization', active: true }
    ];
    this.listApiService.searchEmployee(this.searchTerm, this.searchBy).subscribe((dataEmployee: any[]) => {
      this.arrayDataEmployees = dataEmployee;
      // Lakukan sesuatu dengan data yang diterima
    });
    this.listApiService.getAllRole().subscribe((dataRole: any[]) => {
      this.arrayDataRole = dataRole;
    });
    this.listApiService.getAllDepartement().subscribe((dataDepartement: any[]) => {
      this.arrayDataDepartement = dataDepartement;
    });    
  }
  onInput(event: Event) {
    const searchText = (event.target as HTMLInputElement).value;
    this.autocompleteResults = this.arrayDataEmployees.filter((employee: any) =>
      employee.employee_name.toLowerCase().includes(searchText) || employee.employee_code.toLowerCase().includes(searchText)
    );
  }

  onPageChange(event: any) {
    this.page = event.pageIndex + 1;
    this.itemsPerPage = event.pageSize;
    this.getData(this.page, this.itemsPerPage);
  }

  selectResult(result: any) {
    const selectedValue = result.employee_code + ' - ' + result.employee_name;
    this.dataForm.get('employee_code')?.setValue(result.employee_code);
    this.dataForm.get('employee_name')?.setValue(result.employee_name);

    const inputElement = document.getElementById('f-employee_code') as HTMLInputElement;
    inputElement.value = selectedValue;
    this.autocompleteResults = [];
  }

  getData(page: number, itemsPerPage: number) {
    this.listApiService.getAllAuthorizationPaginate(page, itemsPerPage).subscribe((dataAuth: any) => {
      this.dataSource = new MatTableDataSource<any>(dataAuth.map((x: any) => {        
        return {
          ...x,
          selectedPermission: x.selectedPermission ? x.selectedPermission.split(',').map((x: any) => Number(x)) : []
        }
      }));    
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  onDelete(item: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to delete this item!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        const id = item.id;
        this.listApiService.deleteAuthById(id).subscribe(
          (res) => {            
            this.getData(this.page, this.itemsPerPage);
            Swal.fire('Deleted!', 'The item has been deleted.', 'success');
          },
          (err) => {
            this.getData(this.page, this.itemsPerPage);
            Swal.fire('Error', 'An error occurred while deleting the item.', 'error');
          }
        );
      }
    });
  }

  async openFormModal(content: any, data: any = null) {
    const modalRef = this.modalService.open(content);
    if (data) {
      this.dataForm.patchValue(data); // Set the form values based on the provided data
      await new Promise<void>((resolve) => {
        modalRef['_windowCmptRef'].instance.shown.subscribe(() => {
          const inputElement = document.getElementById('f-employee_code') as HTMLInputElement;
          const codeAndName = `${data.employee_code} - ${data.employee_name}`;
          inputElement.value = codeAndName;
          resolve();
        });
      });
    } else {
      this.dataForm.reset(); // Reset the form if no data is provided
    }
          
  }
  

  onFormSubmit(event: Event) {
    event.preventDefault();
    console.log(this.dataForm.value)

    //validation
    if (!this.dataForm.valid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please fill out all required fields!',
      });
      return;
    }

    //build data
    const form_data = this.dataForm.value;
    const id = this.dataForm.value.id;
    console.log(form_data)
    // Add Data
    if (!id) {
      this.listApiService.createAuth(form_data).subscribe((res: any) => {
        if (res.status) {
          this.getData(this.page, this.itemsPerPage);
          Swal.fire('Data Inserted!', 'The item has been inserted', 'success');
          this.modalService.dismissAll();
        } else {
          Swal.fire('Error', 'An error occurred while inserting the item.', 'error');
          console.log('error', res)
        }
      })
      //Edit Data
    } else {
      this.listApiService.updateAuth(id, form_data).subscribe((res: any) => {
        if (res.status) {
          this.getData(this.page, this.itemsPerPage);
          Swal.fire('Data Updated!', 'The item has been updated', 'success');
          this.modalService.dismissAll()
        } else {
          Swal.fire('Error', 'An error occurred while updating the item.', 'error');
          console.log('error', res)
        }
      })
    }
  }

  onFileSelected(event: any, file: string) {
    const formData = new FormData();
    formData.append('file', event.target.files[0]);

    this.apiService.postFile('/storage/upload', formData)
      .subscribe((res: any) => {
        this.dataForm.get(file)?.patchValue(res.filename);
      });
  }
}
