import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ApiService } from 'src/app/core/services/api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  // bread crumb items
  breadCrumbItems!: Array<{}>;

  constructor(private apiService: ApiService, private modalService: NgbModal) {

  }

  displayedColumns: string[] = ['code', 'description', 'status'];
  searchTerm: string = "";
  dataSource = new MatTableDataSource<any>([]);


  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Apps' },
      { label: 'Master Permission', active: true }
    ];
    this.getData();
  }

  getData() {
    this.apiService.get('master/permission').subscribe((res: any) => {
      this.dataSource = new MatTableDataSource<any>(res.data);
      this.dataSource.sort = this.sort; // Assign MatSort
      this.dataSource.paginator = this.paginator; // Assign MatPaginator
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}