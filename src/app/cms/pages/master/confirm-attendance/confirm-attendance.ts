import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ApiService } from 'src/app/core/services/api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ListApisService } from 'src/app/list-apis.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-confirm-attendance',
  templateUrl: './confirm-attendance.html',
  styleUrls: ['./confirm-attendance.scss']
})
export class ConfirmAttendanceComponent {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  displayedColumns: string[] = ['actions', 'employee_code', 'employee_name', 'name_departement', 'code_departement', 'role', 'created_at', 'status'];
  arrListAuth: any = [];
  arrListAuthAtt: any = [];
  originalData: any = [];
  combinedData: any = [];
  page: number = 1;
  searchTerm: string = "";
  dataSource = new MatTableDataSource<any>([]);
  itemsPerPage: number = 10;
  // bread crumb items
  breadCrumbItems!: Array<{}>;
  dataForm = new FormGroup({
    id: new FormControl(null),
    employee_code: new FormControl('', Validators.required),
    employee_name: new FormControl('', Validators.required),
    department_id: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    status: new FormControl('active', Validators.required),
  });
  checkedStatus: { [key: string]: boolean } = {};

  constructor(private route: ActivatedRoute, private listApiService: ListApisService, private apiService: ApiService, private modalService: NgbModal) {
  }

  checkAll() {
    const isAllChecked = this.isAllChecked();
    for (let item of this.combinedData) {
      this.checkedStatus[item.employee_code] = !isAllChecked;
    }
  }

  getDataParticipant() {
    this.route.params.subscribe(params => {
    const meetingId = params['id'];
    this.listApiService.updateInsertAttendance(meetingId, this.originalData).subscribe((response: any) => {
      console.log(response);
      Swal.fire('Data Inserted!', 'The item has been inserted', 'success');
      this.ngOnInit();
    });
  })
  }

  isAllChecked(): boolean {
    return this.combinedData.every((item: { employee_code: string | number; }) => this.checkedStatus[item.employee_code]);
  }

  onPageChange(event: any) {
    this.page = event.pageIndex + 1;
    this.itemsPerPage = event.pageSize;
    this.getData(this.page, this.itemsPerPage);
  }

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Apps' },
      { label: 'Master Tag', active: true }
    ];

    this.route.params.subscribe(params => {
      const meetingId = params['id']; // Pastikan 'id' sesuai dengan parameter yang ada di URL
      this.listApiService.getAllAuthorization().subscribe((dataAllAuth: any[]) => {
        this.arrListAuth = dataAllAuth;
        this.listApiService.getAllAttendanceMeeting(meetingId).subscribe((dataAllAuthAtt: any[]) => {
          this.arrListAuthAtt = dataAllAuthAtt;

          // Gabungkan data dari kedua API
          this.originalData = this.arrListAuth.map((itemAuth: { employee_code: any; }) => ({
            ...itemAuth,
            checked: this.arrListAuthAtt.some((itemAtt: { employee_code: any; }) => itemAtt.employee_code === itemAuth.employee_code)
          }));

          // Inisialisasi status checked
          this.originalData.forEach((item: { employee_code: string | number; checked: boolean; }) => {
            this.checkedStatus[item.employee_code] = item.checked;
          });

          this.filterData();
        });
      });
    });
  }

  getData(page: number, itemsPerPage: number) {
    this.listApiService.getAllAuthorizationPaginate(page, itemsPerPage).subscribe((dataAuth: any) => {
      this.originalData = dataAuth.map((x: any) => {
        return {
          ...x,
          selectedPermission: x.selectedPermission ? x.selectedPermission.split(',').map((x: any) => Number(x)) : []
        };
      });

      // Update status checked
      this.originalData.forEach((item: { checked: boolean; employee_code: string | number; }) => {
        item.checked = this.checkedStatus[item.employee_code];
      });

      this.filterData();
    });
  }

  applyFilter(filterValue: string) {
    this.searchTerm = filterValue;
    this.filterData();
  }

  filterData() {
    this.combinedData = this.originalData.filter((item: { employee_code: string; employee_name: string; name_departement: string; code_departement: string; role: string; }) => {
      const employeeCodeMatch = item.employee_code.toLowerCase().includes(this.searchTerm.toLowerCase());
      const employeeNameMatch = item.employee_name.toLowerCase().includes(this.searchTerm.toLowerCase());
      const departmentNameMatch = item.name_departement.toLowerCase().includes(this.searchTerm.toLowerCase());
      const departmentCodeMatch = item.code_departement.toLowerCase().includes(this.searchTerm.toLowerCase());
      const roleMatch = item.role.toLowerCase().includes(this.searchTerm.toLowerCase());

      return employeeCodeMatch || employeeNameMatch || departmentNameMatch || departmentCodeMatch || roleMatch;
    });

    this.dataSource = new MatTableDataSource<any>(this.combinedData);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }


  onDelete(item: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to delete this item!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        const id = item.id;
        this.listApiService.deleteAuthById(id).subscribe(
          (res) => {
            this.getData(this.page, this.itemsPerPage);
            Swal.fire('Deleted!', 'The item has been deleted.', 'success');
          },
          (err) => {
            this.getData(this.page, this.itemsPerPage);
            Swal.fire('Error', 'An error occurred while deleting the item.', 'error');
          }
        );
      }
    });
  }

  async openFormModal(content: any, data: any = null) {
    if (data) {
      this.dataForm.patchValue(data); // Set the form values based on the provided data
    } else {
      this.dataForm.reset(); // Reset the form if no data is provided
    }

    const modalRef = this.modalService.open(content);

    await new Promise<void>((resolve) => {
      modalRef['_windowCmptRef'].instance.shown.subscribe(() => {
        const inputElement = document.getElementById('f-employee_code') as HTMLInputElement;
        const codeAndName = `${data.employee_code} - ${data.employee_name}`;
        inputElement.value = codeAndName;
        resolve();
      });
    });
  }

  onFormSubmit(event: Event) {
    event.preventDefault();
    console.log(this.dataForm.value);

    // Validation
    if (!this.dataForm.valid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please fill out all required fields!',
      });
      return;
    }

    // Build data
    const form_data = this.dataForm.value;
    const id = this.dataForm.value.id;
    console.log(form_data);
    // Add Data
    if (!id) {
      this.listApiService.createAuth(form_data).subscribe((res: any) => {
        if (res.status) {
          this.getData(this.page, this.itemsPerPage);
          Swal.fire('Data Inserted!', 'The item has been inserted', 'success');
          this.modalService.dismissAll();
        } else {
          Swal.fire('Error', 'An error occurred while inserting the item.', 'error');
          console.log('error', res);
        }
      });
      // Edit Data
    } else {
      this.listApiService.updateAuth(id, form_data).subscribe((res: any) => {
        if (res.status) {
          this.getData(this.page, this.itemsPerPage);
          Swal.fire('Data Updated!', 'The item has been updated', 'success');
          this.modalService.dismissAll();
        } else {
          Swal.fire('Error', 'An error occurred while updating the item.', 'error');
          console.log('error', res);
        }
      });
    }
  }

  onFileSelected(event: any, file: string) {
    const formData = new FormData();
    formData.append('file', event.target.files[0]);

    this.apiService.postFile('/storage/upload', formData)
      .subscribe((res: any) => {
        this.dataForm.get(file)?.patchValue(res.filename);
      });
  }
}
