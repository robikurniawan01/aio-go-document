import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ApiService } from 'src/app/core/services/api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.scss']
})
export class DoctorComponent {
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  // bread crumb items
  breadCrumbItems!: Array<{}>;

  constructor(private apiService: ApiService, private modalService: NgbModal) {

  }

  displayedColumns: string[] = ['name', 'specialist', 'email', 'phone_number', 'therapeutical', 'status', 'actions'];
  searchTerm: string = "";
  dataSource = new MatTableDataSource<any>([]);
  dataTherapeutical = []
  dataForm = new FormGroup({
    id: new FormControl(null),
    name: new FormControl('', Validators.required),
    specialist: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    phone_number: new FormControl('', Validators.required),
    selectedTherapeuticals: new FormControl('', Validators.required),
    status: new FormControl('active', Validators.required),
  });

  ngOnInit() {
    this.breadCrumbItems = [
      { label: 'Apps' },
      { label: 'Master Doctor', active: true }
    ];
    this.getData();

    this.apiService.get('master/therapeutical').subscribe((res: any) => {
      this.dataTherapeutical = res.data
    });
  }

  getData() {
    this.apiService.get('master/doctor').subscribe((res: any) => {

      this.dataSource = new MatTableDataSource<any>(res.data.map((x: any) => {
        return {
          ...x,
          selectedTherapeuticals: x.selectedTherapeutical.split(',').map((x: any) => Number(x))
        }
      }));
      this.dataSource.sort = this.sort; // Assign MatSort
      this.dataSource.paginator = this.paginator; // Assign MatPaginator
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onDelete(item: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to delete this item!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        const id = item.id;
        this.apiService.put(`master/doctor/${id}`, { form_data: { status: 'deleted' } }).subscribe(
          (res) => {
            this.getData();
            Swal.fire('Deleted!', 'The item has been deleted.', 'success');
          },
          (err) => {
            Swal.fire('Error', 'An error occurred while deleting the item.', 'error');
          }
        );
      }
    });
  }

  openFormModal(content: any, data: any = null) {
    if (data) {
      this.dataForm.patchValue(data); // Set the form values based on the provided data
    } else {
      this.dataForm.reset(); // Reset the form if no data is provided
    }
    this.modalService.open(content);
  }

  onFormSubmit(event: Event) {
    event.preventDefault();
    console.log(this.dataForm.value)

    //validation
    if (!this.dataForm.valid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please fill out all required fields!',
      });
      return;
    }

    //build data
    const form_data = this.dataForm.value;
    const id = this.dataForm.value.id;

    //Add Data
    if (!id) {
      this.apiService.post('master/doctor', { form_data }).subscribe((res: any) => {
        if (res.status) {
          this.getData();
          Swal.fire('Data Inserted!', 'The item has been inserted', 'success');
          this.modalService.dismissAll();
        } else {
          Swal.fire('Error', 'An error occurred while inserting the item.', 'error');
          console.log('error', res)
        }
      })
      //Edit Data
    } else {
      this.apiService.put(`master/doctor/${id}`, { form_data }).subscribe((res: any) => {
        if (res.status) {
          this.getData();
          Swal.fire('Data Updated!', 'The item has been updated', 'success');
          this.modalService.dismissAll()
        } else {
          Swal.fire('Error', 'An error occurred while updating the item.', 'error');
          console.log('error', res)
        }
      })
    }
  }

  onFileSelected(event: any, file: string) {
    const formData = new FormData();
    formData.append('file', event.target.files[0]);

    this.apiService.postFile('/storage/upload', formData)
      .subscribe((res: any) => {
        this.dataForm.get(file)?.patchValue(res.filename);
      });
  }


}
