import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.apiService.get('master/therapeutical').subscribe((data:any) => {
      
    });
  }

}
