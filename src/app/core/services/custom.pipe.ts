import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'status_article'
})
export class StatusArticlePipe implements PipeTransform {
    transform(status: string) {
        switch (status) {
            case 'submitted':
                return '<span class="badge badge-label bg-dark"><i class="mdi mdi-circle-medium"></i> Submitted</span>'
            case 'under approval':
                return '<span class="badge badge-label bg-secondary"><i class="mdi mdi-circle-medium"></i> Under Approval</span>'
            case 'approved':
                return '<span class="badge badge-label bg-success"><i class="mdi mdi-circle-medium"></i> Approved</span>'
            default:
                return '-'
        }
    }
}