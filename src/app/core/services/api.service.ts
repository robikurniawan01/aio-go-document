import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';




@Injectable({
  providedIn: 'root'
})
export class ApiService {
    private apiUrl = environment.apiUrl;
    private csrfToken = "";
    constructor(private http: HttpClient) { }

    // Request header options
    public httpGetOptions(): any { 
      let token = localStorage.getItem('token');
      return { 
        headers : new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `${token}`,
        })
      }
    }

    public httpPostOptions(): any { 
      const token = localStorage.getItem('token');
      // this.fetchCsrfToken();
      return { 
        headers : new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `${token}`,
        'x-csrf-token': this.csrfToken,
        })
      }
    }

    // Request header options
    public httpFileOptions(): any { 
      let token = localStorage.getItem('token');
      return { 
        headers : new HttpHeaders({
        'Authorization': `${token}`,
        })
      }
    }

    private handleErrors(error:any) {
        console.log(error)
        if (error == 'Invalid token') {
          // Handle 403 Forbidden error
          console.error('Authentication error: Clearing localStorage...');
          localStorage.clear();
          window.location.reload();
        }
        return throwError(error);
    }

    public get(url:string): Observable<any> {
            return this.http
          .get(`${this.apiUrl}/${url}`, this.httpGetOptions())
          .pipe(catchError(this.handleErrors));
    }

    public post(url:string, data:any): Observable<any> {
      return this.http.post(`${this.apiUrl}/${url}`, data, this.httpPostOptions()).pipe(catchError(this.handleErrors))
    }

    public put(url:string, data:any): Observable<any> {
      return this.http.put(`${this.apiUrl}/${url}`, data, this.httpPostOptions()).pipe(catchError(this.handleErrors))
    }

    public postFile(url:string, data:any): Observable<any> {
      return this.http.post(`${this.apiUrl}/${url}`, data, this.httpFileOptions()).pipe(catchError(this.handleErrors))
    }

    // private fetchCsrfToken() {
    //   // Make a GET request to your server endpoint to obtain the CSRF token
    //   return this.http.get<{ csrfToken: string }>(`${this.apiUrl}/init-post-request`).subscribe(
    //     (response) => {
    //       this.csrfToken = response.csrfToken;
    //     },
    //     (error) => {
    //       console.error('Error fetching CSRF token:', error);
    //     }
    //   );
    // }
}
