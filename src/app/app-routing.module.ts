import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './template-sample/layouts/layout.component';
import { LayoutComponent as CMSLayoutComponent }  from './cms/layouts/layout.component';


// Auth
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  // cms pages
  { path: '', component: CMSLayoutComponent, loadChildren: () => import('./cms/cms.module').then(m => m.CmsModule), canActivate: [AuthGuard] },
  { path: 'auth', loadChildren: () => import('./account/account.module').then(m => m.AccountModule)  },

  //sample pages from original template
  { path: 'pages', component: LayoutComponent, loadChildren: () => import('./template-sample/pages/pages.module').then(m => m.PagesModule), canActivate: [AuthGuard] },
  { path: 'extra-pages', loadChildren: () => import('./template-sample/extraspages/extraspages.module').then(m => m.ExtraspagesModule), canActivate: [AuthGuard] },
  { path: 'landing', loadChildren: () => import('./template-sample/landing/landing.module').then(m => m.LandingModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
